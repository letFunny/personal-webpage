import os
import re
import subprocess

COMMON_FILES_REGEXP = re.compile(r"(.*)\.mustache")
COMMAND = "mustache"
PAGES = "pages/"

def build_command(page, partials):
    command = [COMMAND]
    for file in partials:
        command.append("-p")
        command.append("{}".format(os.path.relpath(file)))
    command.append(os.path.join(page, "dataView.json"))
    command.append(os.path.join(page, "page.mustache"))
    command.append(page.name + ".html")

    return command

files = list(os.scandir(PAGES))
common_partials = list(filter(lambda file: COMMON_FILES_REGEXP.match(file.name), files))
pages = list(filter(lambda el: el.is_dir(), files))
for page in pages:
    print("Found page {}".format(page.name))
    command = build_command(page, common_partials)
    subprocess.run(command)
# mustache -p header.mustache -p head.mustache dataView.json index.mustache index.html
